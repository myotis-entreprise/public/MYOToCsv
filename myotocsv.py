import sys
import os
from myoreader.myoReader import MyoReader


version = "1.1.0"


####################################################################################
def exit_usage() : 
    print("Usage :\tpython " + os.path.basename(sys.argv[0]) + " srcPath destPath")
    print("\tpython " + os.path.basename(sys.argv[0]) + " --version")
    sys.exit(0)

def exit_version() :
    print("myotocsv :" + str(version))
    print("myoreader :" + MyoReader.class_version)
    sys.exit(0)
####################################################################################

#Check argv
if len(sys.argv) == 1 :
    exit_usage()

if sys.argv[1] == "--version" and len(sys.argv) == 2 :
    exit_version()
elif len(sys.argv) != 3 :
    exit_usage()
    

if os.path.isfile(sys.argv[1]) == False :
    print("Bad source path")
    exit_usage()

parser = MyoReader()
parser.parse(sys.argv[1])

if os.path.isfile(sys.argv[2]) == True :
    os.remove(sys.argv[2])

f = open(sys.argv[2], "x+")
#Insert header
f.write("CRC;"+ "0x%04x" % parser.crc + "\r" )
f.write("VERSION;" + str(parser.version) + "\r" )
f.write("DATE;"+ parser.date_time.strftime("%Y/%m/%d %H:%M:%S.%f") + "\r" )
f.write("UUID;"+ "%04x" % parser.uid + "\r" )
f.write("TICK_DURATION;"+ str(parser.tick_time) + "\r" )
f.write("NAME;"+ parser.station_name + "\r" )

#Insert sensors header
f.write("ID;NAME;UNIT;TICK\r" )
for s in parser.sensors :
    f.write(str(s.id) + ";;;" + str(s.saving_tick) +"\r")

f.write("\r")

#Insert metrix
for tick, points in parser.datas.items() :
    l = tick.strftime("%Y/%m/%d %H:%M:%S.%f") + ";"
    for p in points :
        l += str(p) + ";"

    l+='\r'
    f.write(l.replace('.',','))

f.close()

exit(0)


